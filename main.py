from const import COLUMNS
from helper.cars_scoop import CarsScoop
from helper.utils import setup_logger, setup_parser


def main():

    # setup logger and parser
    logger = setup_logger()
    file_path = setup_parser()

    # init an etl wrapper
    logger.info("Setting up CarsScoop...")
    cars_scoop = CarsScoop()
    
    # read data
    logger.info(f"Reading file from {file_path}...")
    df = cars_scoop.read_multiline_json(file_path)

    # split name to brand and model
    logger.info("Splitting brand and model by name...")
    df = cars_scoop.split_brand_and_model(df)

    # sample result
    selected_columns = df.select(COLUMNS.NAME, COLUMNS.BRAND, COLUMNS.MODEL)
    selected_columns.show(3)

    # find brands with most sold
    logger.info("Finding brands with most sold by year...")
    most_sold_df = cars_scoop.find_most_sold_cars(df)
    most_sold_df.show()

    # find fastest acceleration cars
    logger.info("Finding fastest cars by origin...")
    fastest_cars_df = cars_scoop.find_fastest_acceleration_cars(df)
    fastest_cars_df.show()

    logger.info("Saving results as parquet files")
    df = df.repartition(4, COLUMNS.YEAR)
    cars_scoop.save(df, 'base_data')
    cars_scoop.save(most_sold_df, 'most_sold_data')
    cars_scoop.save(fastest_cars_df, 'fastest_acc_data')


if __name__ == '__main__':
    main()
