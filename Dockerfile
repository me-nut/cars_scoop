FROM python:3.9

# Set the working directory to /app
WORKDIR /app

# Install Java
RUN apt-get update && apt-get install -y default-jdk

# Set environment variables for Spark
ENV SPARK_HOME /spark
ENV PATH $PATH:$SPARK_HOME/bin

# Download and install Spark (update the URL and version)
RUN wget https://dlcdn.apache.org/spark/spark-3.4.1/spark-3.4.1-bin-hadoop3.tgz && \
    tar -xvzf spark-3.4.1-bin-hadoop3.tgz && \
    mv spark-3.4.1-bin-hadoop3 $SPARK_HOME && \
    rm spark-3.4.1-bin-hadoop3.tgz

# Copy the current directory contents into the container at /app
COPY . /app

# Install needed packages in requirements.txt
RUN pip install -r requirements.txt

# Run main.py
CMD ["python", "main.py", "-f", "data/cars.json"]
