import os

CUR_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_DIR = os.path.join(CUR_DIR, 'config')
OUTPUT_DIR = os.path.join(CUR_DIR, 'output')

class COLUMNS:
    NAME = 'Name'
    MILES_PER_GALLON = 'Miles_per_Gallon'
    CYLINDERS = 'Cylinders'
    DISPLACEMENT = 'Displacement'
    HORSE_POWER = 'Horsepower'
    WEIGHT = 'Weight_in_lbs'
    ACCELERATION = 'Acceleration'
    YEAR = 'Year'
    ORIGIN = 'Origin'
    BRAND = 'Brand'
    MODEL = 'Model'
    TOTAL_SOLD = 'total_sold'
    RANK = 'rank'
    MAX_ACC = 'max(Acceleration)'
