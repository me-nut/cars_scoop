import os
import logging
from const import COLUMNS, OUTPUT_DIR
from helper.utils import setup_logger
from pyspark.sql.functions import split, year, count, col, rank
from pyspark.sql.window import Window
from pyspark.sql import SparkSession, DataFrame


class CarsScoop:
    """
    A utility class designed to load a dataset containing information about cars
    from a JSON file, perform statistical calculations on the data, 
    and save the results in a Parquet file.
    """

    spark: SparkSession
    logger: logging.Logger

    def __init__(self):
        """ 
        Setup CarsScoop.
        """
        self.logger = setup_logger()
        self._init_spark_session()

    def _init_spark_session(self):
        """ 
        Setup SparkSession.
        """
        spark = SparkSession.builder \
                            .appName("CarsExtraction") \
                            .getOrCreate()
        
        self.spark = spark

    def read_multiline_json(self, file_path: str) -> DataFrame:
        """
        Read data from multiline json file.

        Args:
            file_path (str): the path to the json input file eg, data/cars.json
        
        Returns:
            Dataframe: a dataframe as result
        """
        if not os.path.exists(file_path):
            raise FileNotFoundError(f"The path you specified can't be found: {file_path}")

        try:
            df = self.spark.read.option("multiline","true") \
                                .json(file_path) 
        except:
            self.logger.error("Error in read_multiline_json")
            raise
        return df
    
    def split_brand_and_model(self, df: DataFrame) -> DataFrame:
        """ 
        Split a column 'Name' to 'Brand' and 'Model'.

        Args:
            df (Dataframe): an input dataframe
        
        Returns:
            Datafrane: a dataframe with 2 new columns: ['Brand', 'Model']
            +--------------------+---------+---------------+
            |                Name|    Brand|          Model|
            +--------------------+---------+---------------+
            |   buick skylark 320|    buick|    skylark 320|
            |  plymouth satellite| plymouth|      satellite|
            +--------------------+---------+---------------+
            
        """
        try:
            df = df.withColumn(COLUMNS.BRAND, split(df[COLUMNS.NAME], "\\s+")[0])
            df = df.withColumn(COLUMNS.MODEL, split(df[COLUMNS.NAME], "\\s+", 2)[1])
        except:
            self.logger.error("Error in split_brand_and_model")
            raise

        return df
    
    def find_most_sold_cars(self, df: DataFrame):
        """
        Find most sold cars by brand and year.

        Args:
            df (Dataframe): an input dataframe

        Returns:
            Datafrane: a dataframe with ["Year, "Brand", "total_sold"] 
            of the most sold brand each year
            +----+---------+----------+
            |Year|    Brand|count_sold|
            +----+---------+----------+
            |1970|      amc|         5|
            |1970|     ford|         5|
            |1971|     ford|         4|
        """
        try:
            # extract year from column "Year"
            df = df.withColumn(COLUMNS.YEAR, year(df[COLUMNS.YEAR]))
            df = df.groupBy(COLUMNS.YEAR, COLUMNS.BRAND).agg(count("*").alias(COLUMNS.TOTAL_SOLD))

            window_spec = Window.partitionBy(COLUMNS.YEAR).orderBy(col(COLUMNS.TOTAL_SOLD).desc())
            
            # rank brands with most sold
            df = df.withColumn(COLUMNS.RANK, rank().over(window_spec))
            df = df.filter(col(COLUMNS.RANK) == 1).drop(COLUMNS.RANK)
        except:
            self.logger.error("Error in find_most_sold_cars")
            raise

        return df
    
    def find_fastest_acceleration_cars(self, df: DataFrame):
        """ 
        Find fastest cars by origin.

        Args:
            df (Dataframe): an input dataframe
        
        Returns:
            Datafrane: a dataframe showing fastest acceleration cars by each origin
            +------+-----------------+
            |Origin|max(Acceleration)|
            +------+-----------------+
            |Europe|             24.8|
            |   USA|             22.2|
            | Japan|             21.0|
            +------+-----------------+
        """
        try:
            df = df.withColumn(COLUMNS.ACCELERATION, df[COLUMNS.ACCELERATION].cast("float"))
            df = df.groupBy(COLUMNS.ORIGIN).max(COLUMNS.ACCELERATION).alias(COLUMNS.ACCELERATION)
        except:
            self.logger.error("Error in find_fastest_acceleration_cars")
            raise

        return df
    
    def save(self, df: DataFrame, dirname: str, partition_by: str=None):
        """
        Save a result dataframe as a parquet file.

        Args:
            df (Dataframe): a dataframe to be written as a parquet file
            dirname (str): written dirname eg, fastest_cars
            partition_by (str): a column to be used with partitionBy method
        """
        try:
            output_filepath = os.path.join(OUTPUT_DIR, dirname)
            
            if partition_by:
                df.write.partitionBy(partition_by).mode("overwrite").parquet(output_filepath)
            else:
                df.write.mode("overwrite").parquet(output_filepath)

        except:
            self.logger.error(f"Error while saving a file: {output_filepath}")
            raise
