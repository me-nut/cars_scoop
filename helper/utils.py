import logging
import argparse


def setup_logger() -> logging.Logger:
    """
    Setup python logger with basic configuration.

    Returns:
        logging.Logger: a python custom logger
    """
    logger = logging.getLogger(__name__)
    logger.handlers = []
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)
    return logger

def setup_parser():
    """
    Setup CLI parameters with arguments.

    Returns:
        str: a path to a json input file
    """
    parser = argparse.ArgumentParser(description="A parser to receive a filename")
    parser.add_argument("-f", "--file_path", type=str, help="Path to an input file", required=True)
    args = parser.parse_args()

    file_path = args.file_path
    return file_path
