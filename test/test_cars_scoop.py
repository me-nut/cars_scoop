import pytest
from const import COLUMNS
from helper.cars_scoop import CarsScoop
from pyspark.sql.functions import col


class TestCarsScoop:

    @pytest.fixture(autouse=True)
    def setup(self):
        cars_scoop = CarsScoop()
        file_path = 'test/mock/input/cars.json'
        self.cars_scoop = cars_scoop
        self.df = cars_scoop.read_multiline_json(file_path)
        
    def test_split_brand_and_model(self):
        df = self.df
        df = self.cars_scoop.split_brand_and_model(df)
        df = df.select(
            COLUMNS.NAME, COLUMNS.BRAND, COLUMNS.MODEL
        ).sort(col(COLUMNS.NAME))

        file_path = 'test/mock/output/brand_model.json'
        expected_df = self.cars_scoop.read_multiline_json(file_path)
        expected_df = expected_df.select(
            COLUMNS.NAME, COLUMNS.BRAND, COLUMNS.MODEL
        ).sort(col(COLUMNS.NAME))

        assert df.collect() == expected_df.collect()

    def test_find_most_sold_cars(self):
        df = self.df
        df = self.cars_scoop.split_brand_and_model(df)
        df = self.cars_scoop.find_most_sold_cars(df)
        df = df.select(
            COLUMNS.YEAR, COLUMNS.BRAND, COLUMNS.TOTAL_SOLD
        ).sort(col(COLUMNS.YEAR))

        file_path = 'test/mock/output/most_sold.json'
        expected_df = self.cars_scoop.read_multiline_json(file_path)
        expected_df = expected_df.select(
            COLUMNS.YEAR, COLUMNS.BRAND, COLUMNS.TOTAL_SOLD
        ).sort(col(COLUMNS.YEAR))

        assert df.collect() == expected_df.collect()

    def test_find_fastest_acceleration_cars(self):
        df = self.df
        df = self.cars_scoop.find_fastest_acceleration_cars(df)
        df = df.select(
            COLUMNS.ORIGIN, COLUMNS.MAX_ACC
        ).sort(col(COLUMNS.ORIGIN))

        file_path = 'test/mock/output/fastest_acc.json'
        expected_df = self.cars_scoop.read_multiline_json(file_path)
        expected_df = expected_df.select(
            COLUMNS.ORIGIN, COLUMNS.MAX_ACC
        ).sort(col(COLUMNS.ORIGIN))

        assert df.collect() == expected_df.collect()


if __name__ == "__main__":
    pytest.main()
